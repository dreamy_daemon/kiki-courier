package main

// discount offers master struct
type Offer struct {
	Code               string  `json:"code"`
	DistanceMin        float32 `json:"distanceMin"`
	DistanceMax        float32 `json:"distanceMax"`
	WeightMin          float32 `json:"weightMin"`
	WeightMax          float32 `json:"weightMax"`
	DiscountPercentage float32 `json:"discountPercentage"`
}

type DataMaster struct {
	Offers []Offer `json:"offers"`
}

// input package information struct
type CourierPackage struct {
	ID        string
	Weight    float32
	Distance  float32
	OfferCode string
}

type Cargo struct {
	Weight   float32
	Distance float32
	Packages []string
}

type Vehicle struct {
	Cargo          Cargo
	AvailableAfter float32
	WeightLimit    float32
	AverageSpeed   float32
}

// output calculate cost and discount
type DeliveryCost struct {
	ID       string
	Discount float32
	Cost     float32
}
