package main

import "testing"

type ApplyOfferCase struct {
	arg1 CourierPackage
	arg2 []Offer
	want float32
}

type CalculateDeliveryCostCase struct {
	arg1    CourierPackage
	arg2    float32
	arg3    float32
	want    DeliveryCost
	wantErr bool
}

func TestIsOfferValid(t *testing.T) {
	offers := []Offer{
		{
			Code:               "OFR001",
			DistanceMin:        0.0,
			DistanceMax:        200.0,
			WeightMin:          70.0,
			WeightMax:          200.0,
			DiscountPercentage: 10.0,
		},
		{
			Code:               "OFR002",
			DistanceMin:        50.0,
			DistanceMax:        150.0,
			WeightMin:          100.0,
			WeightMax:          250.0,
			DiscountPercentage: 7.0,
		},
		{
			Code:               "OFR003",
			DistanceMin:        50.0,
			DistanceMax:        250.0,
			WeightMin:          10.0,
			WeightMax:          150.0,
			DiscountPercentage: 5.0,
		},
	}
	cases := []ApplyOfferCase{
		{
			arg1: CourierPackage{
				ID:        "PKG1",
				Distance:  100.0,
				Weight:    150.0,
				OfferCode: "OFR001",
			},
			arg2: offers,
			want: 10.0,
		},
		{
			arg1: CourierPackage{
				ID:        "PKG2",
				Distance:  170.0,
				Weight:    150.0,
				OfferCode: "OFR002",
			},
			arg2: offers,
			want: 0.0,
		},
		{
			arg1: CourierPackage{
				ID:        "PKG3",
				Distance:  200,
				Weight:    75,
				OfferCode: "OFR003",
			},
			arg2: offers,
			want: 5.0,
		},
	}

	for _, element := range cases {
		got := ApplyOffer(element.arg1, element.arg2)
		if got != element.want {
			t.Errorf("wanted: %v, got: %v", element.want, got)
		}
	}

}

func TestCalculateDeliveryCost(t *testing.T) {
	cases := []CalculateDeliveryCostCase{
		{
			arg1: CourierPackage{
				ID:        "pkg1",
				Weight:    10.0,
				Distance:  100.0,
				OfferCode: "OFR003",
			},
			arg2: 100,
			arg3: 5,
			want: DeliveryCost{
				ID:       "pkg1",
				Cost:     665.0,
				Discount: 35.0,
			},
			wantErr: false,
		},
		{
			arg1: CourierPackage{
				ID:        "pkg2",
				Weight:    15.0,
				Distance:  5.0,
				OfferCode: "OFR002",
			},
			arg2: 100,
			arg3: 0,
			want: DeliveryCost{
				ID:       "pkg2",
				Cost:     275.0,
				Discount: 0.0,
			},
			wantErr: false,
		},
		{
			arg1: CourierPackage{
				ID:        "pkg3",
				Weight:    5.0,
				Distance:  5.0,
				OfferCode: "OFR001",
			},
			arg2: 100,
			arg3: 0,
			want: DeliveryCost{
				ID:       "pkg3",
				Cost:     175.0,
				Discount: 0.0,
			},
			wantErr: false,
		},
	}

	for _, element := range cases {
		got, gotErr := CalculateDeliveryCost(element.arg1, element.arg2, element.arg3)

		if element.want != got {
			t.Errorf("wanted: %v, got: %v", element.want, got)
		}

		if (gotErr != nil) != element.wantErr {
			t.Errorf("ReadMaster() error = %v, wantErr %v", gotErr, element.wantErr)
		}
	}
}
