package main

import (
	"encoding/csv"
	"encoding/json"
	"os"
	"strconv"
)

func ApplyOffer(cpack CourierPackage, offers []Offer) float32 {
	for _, element := range offers {
		if element.Code == cpack.OfferCode {
			if !(cpack.Weight >= element.WeightMin && cpack.Weight <= element.DistanceMax) {
				return 0
			}
			if !(cpack.Distance >= element.DistanceMin && cpack.Distance <= element.DistanceMax) {
				return 0
			}
			return element.DiscountPercentage
		}
	}
	return 0
}

func CalculateDeliveryCost(cpack CourierPackage, base float32, discountPercent float32) (DeliveryCost, error) {
	deliveryCost := DeliveryCost{}
	deliveryCost.ID = cpack.ID
	deliveryCost.Cost = base + (cpack.Weight * 10.0) + (cpack.Distance * 5.0)
	deliveryCost.Discount = discountPercent
	if discountPercent != 0 {
		deliveryCost.Discount = (deliveryCost.Cost / 100) * discountPercent
		deliveryCost.Cost = deliveryCost.Cost - deliveryCost.Discount
	}
	return deliveryCost, nil
}

func ReadMasterJSON(filepath string) ([]Offer, error) {
	dataMaster := DataMaster{}
	filehandle, fileErr := os.Open(filepath)
	if fileErr != nil {
		return dataMaster.Offers, fileErr
	}
	fileStat, statErr := filehandle.Stat()
	if statErr != nil {
		return dataMaster.Offers, statErr
	}
	fileBytes := make([]byte, fileStat.Size())
	_, readErr := filehandle.Read(fileBytes)
	if readErr != nil {
		return dataMaster.Offers, readErr
	}
	if unmarshalErr := json.Unmarshal(fileBytes, &dataMaster); unmarshalErr != nil {
		return dataMaster.Offers, unmarshalErr
	}
	return dataMaster.Offers, nil
}

func ReadInputCSV(filepath string) ([]CourierPackage, error) {
	fileHandle, fileErr := os.Open(filepath)
	if fileErr != nil {
		return []CourierPackage{}, fileErr
	}
	lines, csvReadErr := csv.NewReader(fileHandle).ReadAll()
	if csvReadErr != nil {
		return []CourierPackage{}, csvReadErr
	}
	courierPackages := make([]CourierPackage, len(lines))
	for ix, element := range lines {
		weight, err := strconv.ParseFloat(element[1], 32)
		if err != nil {
			return courierPackages, err
		}
		distance, err := strconv.ParseFloat(element[2], 32)
		if err != nil {
			return courierPackages, err
		}
		courierPackages[ix] = CourierPackage{
			ID:        element[0],
			Weight:    float32(weight),
			Distance:  float32(distance),
			OfferCode: element[3],
		}
	}
	return courierPackages, nil
}

func LoadCargo(vehicle *Vehicle, cargo Cargo, remainingPackages map[string]bool) bool {
	returnVal := false
	loadedCargo := vehicle.Cargo
	for _, pack := range cargo.Packages {
		if !(remainingPackages[pack]) {
			return returnVal
		}
	}
	if len(loadedCargo.Packages) < len(cargo.Packages) {
		loadedCargo = cargo
		returnVal = true
	} else if loadedCargo.Weight < cargo.Weight && len(loadedCargo.Packages) == len(cargo.Packages) {
		loadedCargo = cargo
		returnVal = true
	} else if loadedCargo.Distance > cargo.Distance && loadedCargo.Weight == cargo.Weight && len(loadedCargo.Packages) == len(cargo.Packages) {
		loadedCargo = cargo
		returnVal = true
	}
	vehicle.Cargo = loadedCargo
	return returnVal
}

func PrepareCargos(courierPackages []CourierPackage) []Cargo {
	cargos := []Cargo{}
	for i := 0; i < len(courierPackages); i++ {
		for j := i; j < len(courierPackages)+1; j++ {
			if i != j {
				cargo := Cargo{}
				for ix, element := range courierPackages[i:j] {
					if ix == 0 {
						cargo.Weight = element.Weight
						cargo.Distance = element.Distance
						cargo.Packages = append(cargo.Packages, element.ID)
					} else {
						if cargo.Weight+element.Weight > 200 {
							break
						}
						cargo.Weight = cargo.Weight + element.Weight
						if cargo.Distance < element.Distance {
							cargo.Distance = element.Distance
						}
						cargo.Packages = append(cargo.Packages, element.ID)
					}
				}
				cargos = append(cargos, cargo)
			}
		}
	}
	return cargos
}
