package main

import (
	"log"
	"sort"
)

const BASE = 100

func main() {
	courierPackages, err := ReadInputCSV("./data/input2.csv")
	remainingPackages := map[string]bool{}
	for _, element := range courierPackages {
		remainingPackages[element.ID] = true
	}
	if err != nil {
		log.Fatal(err)
	}
	sort.Slice(courierPackages[:], func(i, j int) bool {
		return courierPackages[i].Weight < courierPackages[j].Weight
	})

	vehicles := []Vehicle{
		{
			Cargo:          Cargo{},
			AvailableAfter: 0,
			WeightLimit:    200,
			AverageSpeed:   70,
		},
		{
			Cargo:          Cargo{},
			AvailableAfter: 0,
			WeightLimit:    200,
			AverageSpeed:   70,
		},
	}
	cargos := PrepareCargos(courierPackages)

	// offers, masterErr := ReadMasterJSON("./data/data.json")
	// if masterErr != nil {
	// 	log.Fatal(masterErr)
	// }
	// for _, item := range offers {
	// 	log.Println(item)
	// }
	// for _, element := range courierPackages {
	// 	discountPercentage := ApplyOffer(element, offers)
	// 	deliveryCost, costCalculationErr := CalculateDeliveryCost(element, BASE, discountPercentage)
	// 	if costCalculationErr != nil {
	// 		log.Fatal(costCalculationErr)
	// 	}
	// 	log.Println(discountPercentage)
	// 	log.Println(deliveryCost)

	// }
}
